package org.formation.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/gitlab")
public class HelloGitlabController {

	
	@GetMapping
	public String helloGitlab() {
		return "bonjour";
	}
}
